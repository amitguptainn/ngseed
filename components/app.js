﻿define(['angular', 'angular-ui-router'], function () {
    var app = angular.module("app", ['ui.router']);
    app.controller("appCtrl", ['$scope', function ($scope) {
        $scope.title = "Hello!"
    }])
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state("header", {
            templateUrl:"../partials/_header.html"
        })
    }])
    return app;
    
})