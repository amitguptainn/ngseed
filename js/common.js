define(['module','jquery','bootstrap-js','css!/css/style'],function(module){
    $(document).ready(function () {
        var homePartial=module.config().homePartial;
        for (partial of homePartial) {
            $(partial.sectionId).load(partial.partialUrl)
            console.log(partial.sectionId)
        }
	
    })
})