﻿require.config({
    paths: {
        "jquery": "bower_components/jquery/dist/jquery",
        "common": "js/common",
        "css":"bower_components/require-css/css",
        "bootstrap-js": "bower_components/bootstrap/dist/js/bootstrap",
        "angular": "bower_components/angular/angular",
        "angular-ui-router":"bower_components/angular-ui-router/release/angular-ui-router",
        "app":"components/app"
    },
    shim: {
        'bootstrap-js': {
            deps:['jquery']
        },
        'jquery': {
            exports: 'jquery',
        },
        'angular':{
            exports:'ng'
        },
        'angular-ui-router': {
            deps:['angular']
        }
    }
})
require(['angular', 'angular-ui-router', 'app'], function () {
    angular.bootstrap(angular.element(document),['app'])
})